const Webpack = require("webpack");
const CopyPlugin = require("copy-webpack-plugin");
const path = require("path");

module.exports = {
    mode: "development",
    entry: {
        "bundle": path.join(__dirname, "src", "web"),
    },
    output: {
        path: path.join(__dirname, "dist"),
    },
    resolve: {
        extensions: [".js", ".jsx", ".ts", ".tsx", ".json", ".scss"],
    },
    module: {
        rules: [
            {
                test: /\.tsx?/,
                loader: "ts-loader",
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                    { loader: "resolve-url-loader" },
                    { loader: "sass-loader" }
                ]
            }
        ]
    },
    externals: {
        "fs": "{}",
        "react-native-sqlite-storage": "{}",
    },
    devtool: "source-map",
    plugins: [
        new CopyPlugin([
            {
                from: path.join(__dirname, "node_modules", "sql.js", "dist", "sql-wasm.wasm"),
                to: path.join(__dirname, "dist"),
            },
            {
                from: path.join(__dirname, "src", "web", "index.html"),
                to: path.join(__dirname, "dist"),
            },
        ]),
    ]
};
