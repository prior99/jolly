FROM node:10-stretch
ARG USER_ID=1000
ARG GROUP_ID=1000
ARG USER_NAME=jolly

# Install all dependencies used for puppeteer and for running the bot.
RUN apt-get -qq update && \
    apt-get -qq install -y gconf-service libasound2 libgconf-2-4 libgtk-3-0 libnspr4 libx11-xcb1 \
      libxss1 libxtst6 fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst ttf-freefont \
      fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils libpango1.0-dev libgif-dev \
      build-essential

RUN groupadd -g $GROUP_ID -o $USER_NAME
RUN useradd -m -u $USER_ID -g $GROUP_ID -o -s /bin/bash $USER_NAME

WORKDIR /jolly

RUN chown $USER_ID:$GROUP_ID /jolly

USER $USER_NAME

# Copy all files which will likely change very infrequently.
COPY ./jolly /jolly/jolly
COPY ./tsconfig.json /jolly/tsconfig.json
COPY ./tslint.json /jolly/tslint.json
COPY ./.stylelintrc.json /jolly/.stylelintrc.json
COPY ./fixtures /jolly/fixtures
COPY ./Makefile /jolly/Makefile

# Install all dependencies.
COPY ./package.json /jolly/package.json
COPY ./yarn.lock /jolly/yarn.lock
RUN yarn

# Copy files changing frequently.
COPY ./test /jolly/test
COPY ./src /jolly/src

RUN yarn build
