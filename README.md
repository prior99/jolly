# jolly

Jolly is a tool for statically generating galleries from images.
It will resize the images, generate a HTML/CSS based slideshow, gallery and index page.
The JS is solely evaluated when executing the CLI and the runtime is free of JS.

## Usage

### Initialize a new site

```
jolly initialize /web-root
```

### Create a new gallery

```
jolly gallery create --name "Example" --description "An example" ~/my-photos /web-root
```

### List all galleries

```
jolly list-galleries /web-root
```

### Add an image to an existing gallery

```
jolly gallery add-image ~/new-photo.jpg /web-root 7a244b9626e8bc89ef7713233260d9df0da63e8032b10405f7b5b98d269912a7
```

### Delete an image from a gallery

```
jolly gallery delete-image /web-root 7a244b9626e8bc89ef7713233260d9df0da63e8032b10405f7b5b98d269912a7
```

### Delete an entire gallery

```
jolly gallery delete /web-root 7a244b9626e8bc89ef7713233260d9df0da63e8032b10405f7b5b98d269912a7
```

### List all photos in a gallery

```
jolly gallery list /web-root 7a244b9626e8bc89ef7713233260d9df0da63e8032b10405f7b5b98d269912a7
```

## Contributing

### Testing

This will execute unit tests and integration tests.
Integration tests will only pass on linux.

```
make docker-test
```

You can execute only the unit-tests without docker like this:

```
make unit-test
```

You can update all snapshots with this target:

```
make docker-test-update
```

### Linting

This will lint both typescript and sass.

```
make lint
```

### Demo

This will create a sample site in `demo/` with two galleries.

```
make demo
```

The demo can be dynamically recreated on file-update:

```
make watch
```

## Contributors

 - Frederick Gnodtke (Prior)