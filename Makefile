SHELL:=/bin/bash
USER_ID:=$$(id -u)
GROUP_ID:=$$(id -g)
DOCKER_CMD:=\
	docker run\
    --rm\
    --interactive\
    --tty\
    --volume ${PWD}/test:/jolly/test\
    --volume ${PWD}/coverage:/jolly/coverage\
    --volume ${PWD}/jest-screenshot-report:/jolly/jest-screenshot-report\
    --user ${USER_ID}:${GROUP_ID}\
	jolly:latest

default: demo

.PHONY: demo
demo: node_modules clean build
	mkdir demo
	./jolly initialize demo
	./jolly gallery create --description "An exemplary gallery with multiple images of which some are the same and some are not." fixtures/images/session1 demo
	./jolly gallery create --description "Another example of a gallery with some images." fixtures/images/session2 demo
	yarn serve demo

.PHONY: unit-test
unit-test: node_modules
	yarn test --testPathIgnorePatterns ".*integration.*"

.PHONY: unit-test-update
unit-test-update: node_modules
	yarn test --testPathIgnorePatterns ".*integration.*" -u

.PHONY: clean
clean:
	rm -rf demo dist

.PHONY: lint
lint: node_modules
	yarn lint

.PHONY: build
build: node_modules
	yarn build

.PHONY: docker
docker:
	docker build -t jolly --build-arg USER_ID=${USER_ID} --build-arg GROUP_ID=${GROUP_ID} .

.PHONY: docker-test
docker-test: docker report-dirs
	${DOCKER_CMD} yarn test

.PHONY: docker-test-update
docker-test-update: docker report-dirs
	${DOCKER_CMD} yarn test -u

.PHONY: all-tests
all-tests: node_modules build
	yarn test

.PHONY: report-dirs
report-dirs:
	mkdir -p coverage jest-screenshot-report

.PHONY: node_modules
node_modules:
	yarn install
