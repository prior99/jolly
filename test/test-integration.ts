jest.unmock("sharp");
jest.mock("../src/utils/logger");

import { launch, Browser, Page } from "puppeteer";
import { v4 as uuidv4 } from "uuid";
import { mkdirp, remove, existsSync } from "fs-extra";
import * as path from "path";
import "jest-screenshot";
import CommandInitialize from "../src/commands/initialize";
import CommandGalleryCreate from "../src/commands/gallery/create";
import { openDatabase, imageSizes  } from "../src/utils";
import { Gallery, Image } from "../src/models";

describe("Integration", () => {
    let browser: Browser;
    let page: Page; let tempDir: string;

    beforeAll(async () => {
        try {
            browser = await launch({
                args: ["--no-sandbox", "--disable-setuid-sandbox"],
                headless: process.env.NO_HEADLESS !== "true",
            });
            page = await browser.newPage();
            page.setViewport({ width: 1280, height: 720 });

            tempDir = path.resolve(path.join(".", "tmp", uuidv4()));
            await mkdirp(tempDir);
        } catch (err) {
            console.error("An error occured when creating the puppeteer instance: ", err);
            throw err;
        }
    });

    describe("after initializing a site", () => {
        beforeAll(async () => {
            try {
                await (new CommandInitialize()).execute(tempDir);
            } catch (err) {
                console.error("An error occured when initializing the site: ", err);
                throw err;
            }
        });

        it("creates a database", () => expect(existsSync(path.join(tempDir, "database.sqlite3"))).toBe(true));

        it("creates an index.html", () => expect(existsSync(path.join(tempDir, "index.html"))).toBe(true));

        it("creates a bundle.css", () => expect(existsSync(path.join(tempDir, "bundle.css"))).toBe(true));

        it("matches the screenshot", async () => {
            await page.goto(`file://${tempDir}/index.html`, { waitUntil: "networkidle0" });
            expect(await page.screenshot({ fullPage: true })).toMatchImageSnapshot();
        });

        describe("after creating a first gallery", () => {
            const someName = "some name";
            const someDescription = "Lorem ipsum dolor sit amet.";

            let gallery1: Gallery;
            let gallery1Dir: string;

            beforeAll(async () => {
                try {
                    await (new CommandGalleryCreate()).execute(
                        path.join(__dirname, "..", "fixtures", "images", "session1"),
                        tempDir,
                        { name: someName, description: someDescription },
                    );
                } catch (err) {
                    console.error("An error occured when creating the gallery: ", err);
                    throw err;
                }

                const db = await openDatabase(tempDir);
                gallery1 = await db.getRepository(Gallery).findOne({
                    where: { name: someName },
                    relations: ["images"],
                });
                await db.close();

                gallery1Dir = path.join(tempDir, gallery1.id);
            });

            it("creates a gallery directory", () => expect(existsSync(gallery1Dir)).toBe(true));

            it("creates a gallery.html", () => expect(existsSync(path.join(gallery1Dir, "gallery.html"))).toBe(true));

            describe.each([[0], [1], [2], [3]])("image %#", (imageIndex: number) => {
                let image: Image;
                let imageDir: string;

                beforeAll(() => {
                    image = gallery1.images[imageIndex];
                    imageDir = path.join(gallery1Dir, "images", image.id);
                });

                it("creates the image dir", () => expect(existsSync(imageDir)).toBe(true));

                it("creates the image.html", () => expect(existsSync(path.join(imageDir, "image.html"))).toBe(true));

                it("creates a copy of the original image", () => {
                    expect(
                        existsSync(path.join(imageDir, `${image.id}_original${path.extname(image.filename)}`)),
                    ).toBe(true);
                });

                it.each(imageSizes.map(size => [size]))(
                    "creates a resized version with a height of %i pixels", (size: number) => {
                        expect(existsSync(path.join(imageDir, `${image.id}_${size}.jpeg`))).toBe(true);
                    },
                );
            });

            it("creates a gallery.html", () => expect(existsSync(path.join(gallery1Dir, "gallery.html"))).toBe(true));

            describe("on the index page", () => {
                beforeAll(async () => await page.goto(`file://${tempDir}/index.html`, { waitUntil: "networkidle0" }));

                it("matches the screenshot", async () => {
                    expect(await page.screenshot({ fullPage: true })).toMatchImageSnapshot();
                });

                describe("when going on a gallery page", () => {
                    beforeAll(async () => {
                        await Promise.all([
                            page.click(".IndexGalleryThumb__link:first-child"),
                            page.waitForNavigation({ waitUntil: "networkidle0" }),
                        ]);
                    });

                    it("matches the screenshot", async () => {
                        expect(await page.screenshot({ fullPage: true })).toMatchImageSnapshot();
                    });

                    describe("when entering an image page", () => {
                        beforeAll(async () => {
                            await Promise.all([
                                page.click(".GalleryImageThumb__link:first-child"),
                                page.waitForNavigation({ waitUntil: "networkidle0" }),
                            ]);
                        });

                        it("matches the screenshot", async () => {
                            expect(await page.screenshot()).toMatchImageSnapshot();
                        });

                        describe("when hovering the filmroll", () => {
                            beforeAll(async () => {
                                await page.hover(".GalleryFilmroll");
                                await page.click(".GalleryFilmroll");
                                await page.waitFor(1200);
                            });

                            it("matches the screenshot", async () => {
                                expect(await page.screenshot()).toMatchImageSnapshot();
                            });
                        });
                    });
                });
            });

        });
    });

    afterAll(async () => {
        await browser.close();
        await remove(tempDir);
    });
});
