import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { GalleryFilmroll } from "../../src/components";
import { Image, Gallery } from "../../src/models";
import { mockAppInfo, mockGallery, mockImage1, mockImage2 } from "../mocks";

describe("GalleryFilmroll", () => {
    describe.each([
        [
            mockImage1,
            {
                ...mockGallery,
                images: [
                    { ...mockImage2, id: "previous-id" },
                    mockImage1,
                    { ...mockImage2, id: "next-id" },
                ],
            },
        ],
    ])("test set %#", (image: Image, gallery: Gallery) => {
        let component: ShallowWrapper<GalleryFilmroll>;

        beforeEach(() => {
            component = shallow(
                <GalleryFilmroll className="test-class" image={image} gallery={gallery} />,
                { context: mockAppInfo },
            );
        });

        it("matches the snapshot", () => expect(component).toMatchSnapshot());

        it("has link to next image", () => {
            expect(component.find(".GalleryFilmroll__nextLink").prop("href")).toBe("../next-id/image.html");
        });

        it("has link to previous image", () => {
            expect(component.find(".GalleryFilmroll__previousLink").prop("href")).toBe("../previous-id/image.html");
        });

        it("has link to gallery", () => {
            expect(component.find(".GalleryFilmroll__galleryLink").prop("href")).toBe("../../gallery.html");
        });

        it.each(gallery.images.map(galleryImage => [galleryImage]))("has image %#", (galleryImage: Image) => {
            expect(component.findWhere(wrapper => wrapper.prop("image") === galleryImage).exists()).toBe(true);
        });

        it("has active image thumb for current image", () => {
            expect(component.findWhere(wrapper => wrapper.prop("image") === image).prop("active")).toBe(true);
        });
    });
});
