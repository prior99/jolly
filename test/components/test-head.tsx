import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { Head } from "../../src/components";
import { mockAppInfo } from "../mocks";

describe("Head", () => {
    describe.each([
        ["some title"],
    ])("with title %s", (title: string) => {
        let component: ShallowWrapper<Head>;

        beforeEach(() => {
            component = shallow(
                <Head title={title} />,
                { context: mockAppInfo },
            );
        });

        it("matches the snapshot", () => expect(component).toMatchSnapshot());
    });
});
