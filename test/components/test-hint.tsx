import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { Hint } from "../../src/components";

describe("Hint", () => {
    describe.each([[true], [false] ])("with direction %s", (vertical: boolean) => {
        let component: ShallowWrapper<Hint>;

        beforeEach(() => component = shallow(<Hint vertical={vertical} className="some-class" />));

        it("matches the snapshot", () => expect(component).toMatchSnapshot());
    });
});
