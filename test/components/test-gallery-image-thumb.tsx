import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { GalleryImageThumb } from "../../src/components";
import { mockImage1, mockAppInfo } from "../mocks";
import { Image } from "../../src/models";

describe("GalleryImageThumb", () => {
    describe.each([[mockImage1]])("test set %#", (image: Image) => {
        let component: ShallowWrapper<GalleryImageThumb>;

        beforeEach(() => component = shallow(<GalleryImageThumb image={image} />, { context: mockAppInfo }));

        it("matches the snapshot", () => expect(component).toMatchSnapshot());

        it("has link to image", () => {
            expect(component.find(".GalleryImageThumb__link").prop("href")).toBe(`images/${image.id}/image.html`);
        });

        it("has source setted image", () => {
            expect(component.find(".GalleryImageThumb__image").prop("image")).toBe(image);
        });
    });
});
