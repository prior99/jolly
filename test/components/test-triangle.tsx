import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { TriangleProps, Triangle } from "../../src/components";

describe("Triangle", () => {
    describe.each([
        ["up"], ["down"], ["left"], ["right"],
    ])("with direction %s", (direction: TriangleProps["direction"]) => {
        let component: ShallowWrapper<Triangle>;

        beforeEach(() => component = shallow(<Triangle direction={direction} />));

        it("matches the snapshot", () => expect(component).toMatchSnapshot());
    });
});
