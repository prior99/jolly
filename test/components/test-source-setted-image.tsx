import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { SourceSettedImage } from "../../src/components";
import { Image } from "../../src/models";
import { imageSizeHD } from "../../src/utils";
import { mockAppInfo, mockImage1 } from "../mocks";

describe("SourceSettedImage", () => {
    describe.each([[mockImage1]])("test set %#", (image: Image) => {
        let component: ShallowWrapper<SourceSettedImage>;

        beforeEach(() => {
            component = shallow(<SourceSettedImage image={image} className="some-class" />, { context: mockAppInfo });
        });

        it("matches the snapshot", () => expect(component).toMatchSnapshot());

        it("image has src", () => {
            expect(component.find("img").prop("src")).toBe(`../../${image.gallery.id}/images/${image.id}/${image.id}_${imageSizeHD}.jpeg`);
        });
    });
});
