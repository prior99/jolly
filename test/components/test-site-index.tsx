import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { SiteIndex } from "../../src/components";
import { mockGallery, mockAppInfo  } from "../mocks";
import { Gallery } from "../../src/models";

describe("SiteIndex", () => {
    describe.each([
        [[
            { ...mockGallery, id: "gallery1", name: "Gallery 1" },
            { ...mockGallery, id: "gallery2", name: "Gallery 2" },
            { ...mockGallery, id: "gallery3", name: "Gallery 3" },
        ]],
    ])("test set %#", (galleries: Gallery[]) => {
        let component: ShallowWrapper<SiteIndex>;

        beforeEach(() => component = shallow(<SiteIndex galleries={galleries} />, { context: mockAppInfo }));

        it("matches the snapshot", () => expect(component).toMatchSnapshot());

        it("has link to all galleries", () => {
            galleries.forEach(gallery => {
                expect(component.findWhere(wrapper => wrapper.prop("gallery") === gallery).exists()).toBe(true);
            });
        });
    });
});
