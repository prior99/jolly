import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { MosaicProps, Mosaic } from "../../src/components";

describe("Mosaic", () => {
    describe.each([["small"], ["large"] ])("with direction %s", (size: MosaicProps["size"]) => {
        let component: ShallowWrapper<Mosaic>;

        beforeEach(() => component = shallow(<Mosaic size={size} className="some-class-name" />));

        it("matches the snapshot", () => expect(component).toMatchSnapshot());
    });
});
