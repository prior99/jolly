import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { FullscreenImage, AppInfoContext  } from "../../src/components";
import { Image, Gallery  } from "../../src/models";
import { mockImage1, mockImage2, mockAppInfo, mockGallery } from "../mocks";

describe("FullscreenImage", () => {
    describe.each([
        [
            mockImage1,
            {
                ...mockGallery,
                images: [mockImage1, mockImage2],
            },
        ],
    ])("test set %#", (image: Image, gallery: Gallery) => {
        let component: ShallowWrapper<FullscreenImage>;

        beforeEach(() => {
            component = shallow(
                <FullscreenImage image={image} gallery={gallery} />,
                { context: mockAppInfo },

            );
        });

        it("matches the snapshot", () => expect(component).toMatchSnapshot());

        it("has link to next image", () => {
            expect(component.find("a").prop("href")).toBe(`../${mockImage2.id}/image.html`);
        });

        it("has source setted image", () => {
            expect(component.find(".FullscreenImage__image").prop("image")).toBe(image);
        });

        it("has filmroll", () => {
            const filmroll = component.find(".FullscreenImage__filmroll");
            expect(filmroll.prop("image")).toBe(image);
            expect(filmroll.prop("gallery")).toBe(gallery);
        });
    });
});
