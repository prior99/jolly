import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { GalleryOverview } from "../../src/components";
import { mockAppInfo, mockGallery, mockImage1, mockImage2 } from "../mocks";
import { Image, Gallery  } from "../../src/models";

describe("GalleryOverview", () => {
    describe.each([
        [
            {
                ...mockGallery,
                images: [mockImage1, mockImage2],
            },
        ],
    ])("test set %#", (gallery: Gallery) => {
        let component: ShallowWrapper<GalleryOverview>;

        beforeEach(() => {
            component = shallow(<GalleryOverview gallery={gallery} />, { context: mockAppInfo });
        });

        it("matches the snapshot", () => expect(component).toMatchSnapshot());

        it.each(gallery.images.map(galleryImage => [galleryImage]))("has image %#", (galleryImage: Image) => {
            expect(component.findWhere(wrapper => wrapper.prop("image") === galleryImage).exists()).toBe(true);
        });

        it("has link to index", () => {
            expect(component.find(".GalleryOverview__indexLink").prop("href")).toBe("../index.html");
        });
    });
});
