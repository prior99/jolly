import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { ImageThumb } from "../../src/components";
import { Image } from "../../src/models";
import { imageSizeTiny } from "../../src/utils";
import { mockImage1, mockAppInfo  } from "../mocks";

describe("ImageThumb", () => {
    describe.each([[mockImage1, false], [mockImage1, true]])("test set %#", (image: Image, active: boolean) => {
        let component: ShallowWrapper<ImageThumb>;

        beforeEach(() => component = shallow(<ImageThumb image={image} active={active} />, { context: mockAppInfo }));

        it("matches the snapshot", () => expect(component).toMatchSnapshot());

        it("has link to image", () => {
            expect(component.find(".ImageThumb__link").prop("href")).toBe(`../${image.id}/image.html`);
        });

        it("has image", () => {
            expect(component.find(".ImageThumb__image").prop("src")).toBe(`../../${image.gallery.id}/images/${image.id}/${image.id}_${imageSizeTiny}.jpeg`);
        });
    });
});
