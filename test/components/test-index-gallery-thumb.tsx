import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { IndexGalleryThumb } from "../../src/components";
import { imageSizeTiny } from "../../src/utils";
import { Gallery } from "../../src/models";
import { mockGallery, mockAppInfo } from "../mocks";

describe("IndexGalleryThumb", () => {
    describe.each([[mockGallery]])("test set %#", (gallery: Gallery) => {
        let component: ShallowWrapper<IndexGalleryThumb>;

        beforeEach(() => component = shallow(<IndexGalleryThumb gallery={gallery} />, { context: mockAppInfo }));

        it("matches the snapshot", () => expect(component).toMatchSnapshot());

        it("has link to gallery", () => {
            expect(component.find(".IndexGalleryThumb__link").prop("href")).toBe(`../../${gallery.id}/gallery.html`);
        });

        it("has gallery name", () => {
            expect(component.find(".IndexGalleryThumb__name").text()).toBe(gallery.name);
        });

        it("has preview images", () => {
            gallery.images.forEach(image => {
                expect(component.findWhere(wrapper => {
                    return wrapper.prop("src") ===
                        `../../${gallery.id}/images/${image.id}/${image.id}_${imageSizeTiny}.jpeg`;
                }).exists()).toBe(true);
            });
        });
    });
});
