import { configure } from "enzyme";
import * as Adapter from "enzyme-adapter-react-16";
import { setupJestScreenshot } from "jest-screenshot";
 
setupJestScreenshot();

beforeAll(() => {
    configure({ adapter: new Adapter() });
});

jest.setTimeout(600000);
