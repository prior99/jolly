jest.mock("../../src/utils/logger");
jest.mock("sharp");

import * as fs from "fs-extra";
import * as sharp from "sharp";
import * as path from "path";
import * as processImageModule from "../../src/utils/process-image";
import { processImages } from "../../src/utils";

describe("processImages", () => {
    const someWidth = 200;
    const someHeight = 100;
    const someFormat = "png";
    const someSourceDir = "/tmp/source";
    const someTargetPath = "/tmp/target";
    const someFilenames = ["image-1.jpg", "image-2.jpg", "image-3.jpg"];
    const imageIds = new Map<string, string>([
        ["image-1.jpg", "abd47f1d683b2c86321ab8e328a3086e479d756c6dfb45084584341ec005ca98"],
        ["image-2.jpg", "483e6709e81fedf31a80f9553a114b5f12501010bf8a8f42667e8fc5307f6cfe"],
        ["image-3.jpg", "0f6baf2c9ac6df8bb7a41f971bbf13c4307397717e8452c233a8c9a43df0ba2e"],
    ]);

    let spyProcessImage: jest.SpyInstance<any>;
    let returnValue: any;

    beforeEach(async () => {
        jest.spyOn(fs, "statSync").mockReturnValue({ isFile: true } as any);
        jest.spyOn(fs, "readdir").mockResolvedValue(someFilenames);
        spyProcessImage = jest.spyOn(processImageModule, "processImage")
            .mockImplementation(async (sourceFile: string) => ({
                id: imageIds.get(path.basename(sourceFile)),
                width: someWidth,
                height: someHeight,
                filename: sourceFile,
                format: someFormat,
            }));

        returnValue = await processImages(someSourceDir, someTargetPath);
    });

    it.each(someFilenames.map(filename => ([filename])))("calls processImage with %s", (filename: string) => {
        expect(spyProcessImage).toHaveBeenCalledWith(`${someSourceDir}/${filename}`, someTargetPath);
    });

    it("returns the expected result", () => {
        expect(returnValue).toEqual(someFilenames.map(filename => ({
            id: imageIds.get(filename),
            filename: `${someSourceDir}/${filename}`,
            width: someWidth,
            height: someHeight,
            format: someFormat,
        })));
    });
});
