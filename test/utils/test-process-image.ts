jest.mock("../../src/utils/logger");
jest.mock("sharp");

import * as fs from "fs-extra";
import * as sharp from "sharp";
import { imageSizes, processImage  } from "../../src/utils";

describe("processImage", () => {
    const someImagePath = "/tmp/example.jpg";
    const someWidth = 200;
    const someHeight = 100;
    const someFormat = "png";
    const imageId = "97bb768c671cc706c9bb144dcfdb53cc678b0e5b619a9cc7746b878c1805f79e";
    const someTargetPath = "/tmp/target";

    let returnValue: any;
    let spyCopy: jest.SpyInstance<any>;
    let spyResize: jest.SpyInstance<any>;
    let spyWebp: jest.SpyInstance<any>;
    let spyToFile: jest.SpyInstance<any>;

    beforeEach(async () => {
        spyCopy = jest.spyOn(fs, "copy").mockReturnValue();
        spyResize = jest.fn().mockReturnThis();
        spyWebp = jest.fn().mockReturnThis();
        spyToFile = jest.fn().mockReturnThis();
        (sharp as jest.SpyInstance<any>).mockReturnValue({
            metadata: jest.fn(() => ({
                width: someWidth,
                height: someHeight,
                format: someFormat,
            })),
            resize: spyResize,
            webp: spyWebp,
            toFile: spyToFile,
        });

        returnValue = await processImage(someImagePath, someTargetPath);
    });

    it("calls sharp", () => expect(sharp).toHaveBeenCalledWith(someImagePath));

    it("calls copy", () => expect(spyCopy).toHaveBeenCalledWith(
        "/tmp/example.jpg",
        `/tmp/target/${imageId}/${imageId}_original.jpg`,
        { dereference: true },
    ));

    it.each(imageSizes.map(size => ([size])))("calls the resize method with size %i", (size: number) => {
        expect(spyResize).toHaveBeenCalledWith({ height: size });
    });

    it.each(imageSizes.map(size => ([size])))("calls the toFile method with size %i", (size: number) => {
        expect(spyToFile).toHaveBeenCalledWith(`/tmp/target/${imageId}/${imageId}_${size}.jpeg`);
    });

    it("returns the expected result", () => {
        expect(returnValue).toEqual({
            filename: "example.jpg",
            id: imageId,
            format: someFormat,
            width: someWidth,
            height: someHeight,
        });
    });
});
