import { AppInfo } from "../../src/utils";

export const mockAppInfo: AppInfo = {
    relativeUrl: "some/path",
};
