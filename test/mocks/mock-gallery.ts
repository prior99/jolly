import { Gallery } from "../../src/models";

export const mockGallery: Gallery = {
    id: "8af7b7bf167ff1219db564a03d9c16d9590d7b86259e2844dfd9a04bd3a039ac",
    name: "Some gallery",
    description: "Some gallery's description.",
    created: new Date("2019-10-10T12:00:00Z"),
    updated: new Date("2019-10-10T12:00:00Z"),
    images: [],
};
