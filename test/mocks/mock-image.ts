import { Image } from "../../src/models";
import { Gallery } from "../../src/models";

export const mockImage1: Image = {
    id: "e01acebbe41fad11f51ffb16a074a795f24162fe749fa314efb4a34796f157a3",
    filename: "some-image.png",
    format: "png",
    width: 800,
    height: 600,
    created: new Date("2019-10-10T12:00:00Z"),
    updated: new Date("2019-10-10T12:00:00Z"),
    gallery: { id: "8af7b7bf167ff1219db564a03d9c16d9590d7b86259e2844dfd9a04bd3a039ac" } as Gallery,
};

export const mockImage2: Image = {
    id: "f6502de01da0eb79cff590653dd2dcb2935e6d437cec350411373a9fec8939fc",
    filename: "some-other-image.jpg",
    format: "png",
    width: 1920,
    height: 1080,
    created: new Date("2019-12-10T12:00:00Z"),
    updated: new Date("2019-12-12T12:00:00Z"),
    gallery: { id: "8af7b7bf167ff1219db564a03d9c16d9590d7b86259e2844dfd9a04bd3a039ac" } as Gallery,
};