import { MigrationInterface, QueryRunner } from "typeorm";
import { logger } from "../cli-utils/logger";

export class Initial1574843862391 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        logger.info("Applying migration: Initial");
        await queryRunner.query(`
            CREATE TABLE gallery (
                id CHARACTER VARYING(100) NOT NULL,
                created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                name TEXT NOT NULL,
                description TEXT,
                CONSTRAINT pk_gallery_id PRIMARY KEY (id)
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "image" (
                id CHARACTER VARYING(100) NOT NULL,
                created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                filename TEXT NOT NULL,
                format CHARACTER VARYING(100) NOT NULL,
                width INT NOT NULL,
                height INT NOT NULL,
                gallery_id CHARACTER VARYING(100) NOT NULL,
                CONSTRAINT pk_image_id PRIMARY KEY (id),
                CONSTRAINT fk_gallery_id FOREIGN KEY (gallery_id) REFERENCES "gallery"(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        logger.info("Reverting migration: Initial");
        await queryRunner.query(`DROP TABLE gallery`);
        await queryRunner.query(`DROP TABLE image`);
    }
}
