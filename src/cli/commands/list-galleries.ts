import { Command, command, param } from "clime";
import { openDatabaseCLI } from "../cli-utils";
import { ModelGallery } from "../../common";

@command({ description: "List all galleries." })
export default class extends Command {
    public async execute(
        @param({
            description: "Directory containing the website.",
            required: true,
        })
        webDir: string,
    ) {
        const db = await openDatabaseCLI(webDir);
        const galleries = await db.getRepository(ModelGallery).find();
        galleries.forEach(gallery => {
            process.stdout.write(`${gallery.id}   ${gallery.name}\n`);
        });

        await db.close();
    }
}
