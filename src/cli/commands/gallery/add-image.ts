import { Command, command, param } from "clime";
import * as path from "path";
import { processImage, openDatabaseCLI   } from "../../cli-utils";
import { imagesDirName, ModelImage  } from "../../../common";

@command({ description: "Add a single image to an existing gallery." })
export default class extends Command {
    public async execute(
        @param({
            description: "The image file to add.",
            required: true,
        })
        sourceFile: string,
        @param({
            description: "Directory containing the website.",
            required: true,
        })
        webDir: string,
        @param({
            description: "Id of the gallery to delete.",
            required: true,
        })
        id: string,
    ) {
        const image = await processImage(sourceFile, path.join(webDir, id, imagesDirName));
        const db = await openDatabaseCLI(webDir);
        await db.getRepository(ModelImage).save({ ...image, gallery: { id } });
        await db.close();
    }
}
