import { Command, command, param } from "clime";
import { openDatabaseCLI } from "../../cli-utils";
import { ModelGallery } from "../../../common";

@command({ description: "List all images within a gallery." })
export default class extends Command {
    public async execute(
        @param({
            description: "Directory containing the website.",
            required: true,
        })
        webDir: string,
        @param({
            description: "Id of the gallery to show.",
            required: true,
        })
        id: string,
    ) {
        const db = await openDatabaseCLI(webDir);
        const gallery = await db.getRepository(ModelGallery).findOne({
            where: { id },
            relations: ["images"],
        });
        gallery.images.forEach(image => {
            process.stdout.write(`${image.id}   ${image.filename}\n`);
        });

        await db.close();
    }
}
