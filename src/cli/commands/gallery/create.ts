import { Command, command, param, Options, option } from "clime";
import * as path from "path";
import { mkdirp } from "fs-extra";
import * as crypto from "crypto";
import { processImages, openDatabaseCLI } from "../../cli-utils";
import { ModelGallery, ModelImage, imagesDirName } from "../../../common";

class GenerateOptions extends Options {
    @option({ description: "Gallery name, defaults to directory name of images." })
    public name?: string;

    @option({ description: "Optional gallery description." })
    public description?: string;
}

@command({ description: "Generate a new gallery from a set of images." })
export default class extends Command {
    public async execute(
        @param({
            description: "Directory containing the images.",
            required: true,
        })
        sourceDir: string,
        @param({
            description: "Directory containing the website.",
            required: true,
        })
        webDir: string,
        options: GenerateOptions,
    ) {
        const id = crypto
            .createHash("sha256")
            .update(path.resolve(sourceDir))
            .digest("hex")
            .substr(0, 8);
        const galleryPath = path.join(webDir, id);
        const targetImagesDir = path.join(galleryPath, imagesDirName);
        const db = await openDatabaseCLI(webDir);

        // Create gallery directory.
        await mkdirp(galleryPath);

        // Create gallery in database.
        await db.getRepository(ModelGallery).save({
            id,
            name: options.name || path.basename(sourceDir),
            description: options.description,
        });

        // Create resized images.
        const imageInfos = await processImages(sourceDir, targetImagesDir);

        // Create images in database.
        await Promise.all(imageInfos.map(async image => {
            await db.getRepository(ModelImage).save({ ...image, gallery: { id } });
        }));

        await db.close();
    }
}
