import { Command, command, param } from "clime";
import { remove } from "fs-extra";
import * as path from "path";
import { openDatabaseCLI } from "../../cli-utils";
import { ModelGallery, ModelImage } from "../../../common";

@command({ description: "Delete gallery with given id." })
export default class extends Command {
    public async execute(
        @param({
            description: "Directory containing the website.",
            required: true,
        })
        webDir: string,
        @param({
            description: "Id of the gallery to delete.",
            required: true,
        })
        id: string,
    ) {
        const db = await openDatabaseCLI(webDir);
        await db.getRepository(ModelImage).delete({ gallery: { id } });
        await db.getRepository(ModelGallery).delete({ id });
        await db.close();
        await remove(path.join(webDir, id));
    }
}
