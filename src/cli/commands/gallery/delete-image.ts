import { Command, command, param } from "clime";
import { remove } from "fs-extra";
import * as path from "path";
import { openDatabaseCLI } from "../../cli-utils";
import { ModelImage, imagesDirName } from "../../../common";

@command({ description: "Delete a single image from an existing gallery." })
export default class extends Command {
    public async execute(
        @param({
            description: "Directory containing the website.",
            required: true,
        })
        webDir: string,
        @param({
            description: "The id of the image to delete.",
            required: true,
        })
        id: string,
    ) {
        const db = await openDatabaseCLI(webDir);
        const image = await db.getRepository(ModelImage).findOne({ where: { id }, relations: ["gallery"]});
        await db.getRepository(ModelImage).delete({ id });
        await db.close();
        await remove(path.join(webDir, image.gallery.id, imagesDirName, id));
    }
}
