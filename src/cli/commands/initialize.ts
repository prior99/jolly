import { Command, command, param } from "clime";
import { copy, writeFile } from "fs-extra";
import * as path from "path";
import { openDatabaseCLI, logger } from "../cli-utils";

const resources = [
    "index.html",
    "bundle.js",
    "bundle.js.map",
    "sql-wasm.wasm",
];

@command({
    description: "Generate a site.",
})
export default class extends Command {
    public async execute(
        @param({
            description: "Directory the site should be initialized in.",
            required: true,
        })
        webDir: string,
    ) {
        // Copy resources.
        await Promise.all(resources.map(async resource => {
            await copy(
                path.join(__dirname, "..", "..", resource),
                path.join(webDir, resource),
                { dereference: true },
            );
        }))
        await writeFile(path.join(webDir, "manifest.json"), JSON.stringify({
            short_name: "Gallery",
            name: "Gallery",
            start_url: "./index.html",
            display: "fullscreen",
        }));

        // Initialize database.
        logger.info(`Creating database "${webDir}/database.sqlite3"...`);
        const db = await openDatabaseCLI(webDir);
        await db.close();
    }
}
