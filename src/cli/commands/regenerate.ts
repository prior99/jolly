import { Command, command, param, Options, option } from "clime";
import * as path from "path";
import { processImage, openDatabaseCLI } from "../cli-utils";
import { ModelGallery } from "../../common/models";
import { imagesDirName } from "../../common";

class RegenerateOptions extends Options {
    @option({ toggle: true, description: "Also rescale the images." })
    public rescale: boolean;
}

@command({ description: "Regenerate all pages." })
export default class extends Command {
    public async execute(
        @param({
            description: "Directory containing the website.",
            required: true,
        })
        webDir: string,
        options: RegenerateOptions,
    ) {
        const db = await openDatabaseCLI(webDir);
        const galleries = await db.getRepository(ModelGallery).find({ relations: ["images", "images.gallery"]});
        await db.close();
        for (let gallery of galleries) {
            if (options.rescale) {
                await Promise.all(gallery.images.map(async image => {
                    const fileName = `${image.id}_original${path.extname(image.filename)}`;
                    const imagesPath = path.join(webDir, gallery.id, imagesDirName);
                    const sourceFile = path.join(imagesPath, image.id, fileName);
                    await processImage(sourceFile, imagesPath);
                }));
            }
        }
    }
}
