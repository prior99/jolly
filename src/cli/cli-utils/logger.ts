import * as Winston from "winston";

const transport = new Winston.transports.Console({
    level: "verbose",
    format: Winston.format.combine(
        Winston.format.timestamp({ format: "YYYY-MM-DDTHH:mm:ss" }),
        Winston.format.colorize(),
        Winston.format.printf(param => `${param.timestamp} - ${param.level}: ${param.message}`),
    ),
    stderrLevels: ["debug", "verbose", "info", "warn", "error"],
});

export function setLevel(level: "error" | "info" | "verbose" | "debug" | "warn") {
    transport.level = level;
}

export const logger = Winston.createLogger({ transports: [ transport ] });
