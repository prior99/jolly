import * as sharp from "sharp";
import { mkdirp, copy } from "fs-extra";
import * as path from "path";
import { imageFilename, imageSizes } from "../../common";
import * as crypto from "crypto";
import { logger } from "./logger";

export interface ProcessImageResult {
    filename: string;
    id: string;
    format: string;
    width: number;
    height: number;
}

export async function processImage(
    imagePath: string,
    targetDir: string,
): Promise<ProcessImageResult> {
    logger.info(`Processing image "${imagePath}".`);
    const image = sharp(imagePath);
    const { width, height, format } = await image.metadata();
    const id = crypto
        .createHash("sha256")
        .update(path.resolve(imagePath))
        .digest("hex")
        .substr(0, 8);
    const filename = path.basename(imagePath);

    const imageDir = path.join(targetDir, id);
    mkdirp(imageDir);

    const extension = path.extname(imagePath);
    const targetCopyPath = path.join(imageDir, `${id}_original${extension}`);
    if (targetCopyPath !== imagePath) {
        await copy(imagePath, targetCopyPath, { dereference: true });
    }
    await Promise.all(imageSizes.map(async size => {
        logger.debug(`Generating resolution ${size} for image "${imagePath}".`);
        await sharp(imagePath).resize({ height: size })
            .jpeg({ quality: 95 })
            .toFile(path.join(imageDir, imageFilename(id, size)));
    }));

    logger.debug(`Finished processing "${imagePath}".`);

    return { filename, id, format, width, height };
}
