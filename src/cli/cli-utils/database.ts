import { createConnection } from "typeorm";
import { migrations } from "../migrations";
import { SnakeNamingStrategy, entities  } from "../../common";
import * as path from "path";
import { logger } from "./logger";

export async function openDatabaseCLI(sitePath: string) {
    const database = path.join(sitePath, "database.sqlite3");
    logger.debug(`Opening database "${database}"...`);
    const connection = await createConnection({
        type: "sqlite",
        namingStrategy: new SnakeNamingStrategy(),
        database,
        migrations,
        entities,
    });
    if (await connection.showMigrations()) {
        logger.info("Running neccessary migrations...");
        await connection.runMigrations();
    }
    return connection;
}
