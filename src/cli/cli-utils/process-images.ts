import { readdir, statSync } from "fs-extra";
import { ProcessImageResult, processImage } from "./process-image";
import * as path from "path";
import { logger } from "./logger";

export async function processImages(sourceDir: string, targetDir: string) {
    const imagePaths = (await readdir(sourceDir))
        .map(name => path.join(sourceDir, name))
        .filter(imagePath => statSync(imagePath).isFile);
    // Evaluate the resizing in batches.
    const batchSize = 8;
    let results: ProcessImageResult[] = [];
    for (let i = 0; i < imagePaths.length; i += batchSize) {
        results.push(...(await Promise.all(
            imagePaths
                .slice(i, Math.min(i + batchSize, imagePaths.length))
                .map(async imagePath => {
                    try {
                        return await processImage(imagePath, targetDir);
                    } catch (err) {
                        logger.warn(`Failed to process file "${imagePath}". Perhaps it's not an image?`);
                    }
                }),
        )));
    }
    return results.filter(image => Boolean(image));
}
