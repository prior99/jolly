#!/usr/bin/env node
import * as Path from "path";
import { CLI, Shim } from "clime";

const cli = new CLI("jolly", Path.join(__dirname, "cli", "commands"));
const shim = new Shim(cli);

shim.execute(process.argv);
