import { component, inject } from "tsdi";
import { Connection } from "typeorm";
import { cache } from "../utils/cache";
import { ModelGallery } from "../../common";

@component
export class ServiceGalleries {
    @inject private db: Connection;

    public byId = cache(async (id: string) => await this.db.getRepository(ModelGallery).findOne(id));

    public all = cache(async () => await this.db.getRepository(ModelGallery).find());
}
