import { component, inject } from "tsdi";
import { Connection } from "typeorm";
import { ModelImage } from "../../common";
import { cache } from "../utils/cache";

@component
export class ServiceImages {
    @inject private db: Connection;

    public byId = cache(async (id: string) => await this.db.getRepository(ModelImage).findOne({
        where: { id },
        relations: ["gallery"],
    }));

    public next = cache(async (id: string) => {
        const image = await this.db.getRepository(ModelImage).findOne({
            where: { id },
            relations: ["gallery", "gallery.images"],
        });
        const { images } = image.gallery;
        const ownIndex = images.findIndex(other => other.id === image.id);
        const nextIndex = (ownIndex + 1) % images.length;
        return images[nextIndex];
    });

    public previous = cache(async (id: string) => {
        const image = await this.db.getRepository(ModelImage).findOne({
            where: { id },
            relations: ["gallery", "gallery.images"],
        });
        const { images } = image.gallery;
        const ownIndex = images.findIndex(other => other.id === image.id);
        const previousIndex = ownIndex === 0 ? images.length - 1 : ownIndex - 1;
        return images[previousIndex];
    });

    public inGallery = cache(async (id: string) => await this.db.getRepository(ModelImage).find({
        where: { gallery: { id } },
        order: {
            filename: "ASC",
        },
    }));
}
