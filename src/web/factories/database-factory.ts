import { component, factory } from "tsdi";
import { Connection } from "typeorm";
import { openDatabaseWeb } from "../utils";

@component
export class FactoryDatabase {
    private connection: Connection;

    public async initialize() {
        this.connection = await openDatabaseWeb();
    }

    @factory({ name: "database" })
    public getConnection(): Connection {
        return this.connection;
    }
}
