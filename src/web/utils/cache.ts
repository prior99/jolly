import { observable } from "mobx";

export function cache<TReturn, TParams extends unknown[]> (
    asyncFn: (...params: TParams) => Promise<TReturn>,
): (...params: TParams) => TReturn | undefined {
    const state = observable.map<string, TReturn>();
    return (...params: TParams) => {
        const key = JSON.stringify(params);
        if (state.has(key)) {
            return state.get(key);
        }
        (async () => {
            state.set(key, await asyncFn(...params));
        })();
        return undefined;
    };
}
