import { imagesDirName, imageFilename } from "../../common";

export function imageUrl(galleryId: string, imageId: string, size: number) {
    return `./${galleryId}/${imagesDirName}/${imageId}/${imageFilename(imageId, size)}`;
}