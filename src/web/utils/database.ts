
import { createConnection } from "typeorm";
import { SnakeNamingStrategy, entities  } from "../../common";
import * as sqljs from "sql.js";

export async function openDatabaseWeb() {
    // Load database and wasm module in parallel.
    const [database] = await Promise.all([
        (async () => new Uint8Array(await (await fetch("./database.sqlite3")).arrayBuffer()))(),
        sqljs(),
    ]);
    const connection = await createConnection({
        type: "sqljs",
        namingStrategy: new SnakeNamingStrategy(),
        database,
        entities,
    });
    return connection;
}
