import * as React from "react";
import * as ReactDOM from "react-dom";
import "./index.scss";
import "./factories";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { routeIndex } from "./pages";
import { routes } from "./routing";
import { TSDI } from "tsdi";
import { FactoryDatabase } from "./factories";

(async () => {
    // Setup dependency injection.
    const tsdi = new TSDI();
    tsdi.enableComponentScanner();

    await tsdi.get(FactoryDatabase).initialize();

    // Start React.
    ReactDOM.render(
        <>
            <Router history={tsdi.get("history")}>
                <Switch>
                    <Redirect exact from="/" to={routeIndex.path()} />
                    {
                        routes.map((route, index) => <Route
                            path={route.pattern}
                            component={route.component}
                            key={index}
                        />)
                    }
                </Switch>
            </Router>
        </>,
        document.getElementById("app"),
    );
})();