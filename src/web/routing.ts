import * as React from "react";

export type PathFactory = (...args: string[]) => string;

export interface Route {
    path: PathFactory;
    pattern: string;
    component: React.ComponentClass;
}

export const routes: Route[] = [];

export function addRoute(route: Route) {
    routes.push(route);
}

export interface RouteProps<T> {
    readonly match: {
        readonly params: T;
        readonly url: string;
    };
}
