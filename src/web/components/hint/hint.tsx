import * as React from "react";
import * as classNames from "classnames";
import "./hint.scss";

export interface HintProps {
    vertical?: boolean;
    className?: string;
}

export class Hint extends React.Component<HintProps> {
    get classes() {
        return classNames({
            "Hint": true,
            "Hint--vertical": this.props.vertical,
            "Hint--horizontal": !this.props.vertical,
        });
    }

    public render() {
        return (
            <div className={`${this.classes} ${this.props.className || ""}`}>
                <div className="Hint__dot" />
                <div className="Hint__dot" />
                <div className="Hint__dot" />
            </div>
        );
    }
}
