import * as React from "react";
import { Link } from "react-router-dom";
import { isAfter, format } from "date-fns";
import * as classNames from "classnames";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { GalleryImageThumb } from "../gallery-image-thumb";
import { Mosaic } from "../mosaic";
import { Triangle } from "../triangle";
import { routeIndex } from "../../pages";
import "./gallery-overview.scss";
import { ServiceGalleries, ServiceImages } from "../../services";
import { computed } from "mobx";

@external @observer
export class GalleryOverview extends React.Component<{ id: string }> {
    @inject private serviceGalleries: ServiceGalleries;
    @inject private serviceImages: ServiceImages;

    @computed private get images() {
        return this.serviceImages.inGallery(this.props.id) || [];
    }

    @computed private get gallery() {
        return this.serviceGalleries.byId(this.props.id);
    }

    get lastUpdate() {
        return this.images
            .map(image => image.updated)
            .reduce((result, current) => isAfter(result, current) ? result : current);
    }

    public render() {
        if (!this.gallery || this.images.length === 0) { return <></>; }
        const { name, description } = this.gallery;
        const lastUpdate = format(this.lastUpdate, "yyyy-MM-dd");
        return (
            <article className="GalleryOverview">
                <h2 className="GalleryOverview__title">{name}</h2>
                {
                    <section className="GalleryOverview__description">
                        <p>
                            {description ? description : <></>}
                            <span
                                className={classNames({
                                    "GalleryOverview__date": true,
                                    "GalleryOverview__date--withDescription": Boolean(description),
                                })}
                            >
                                {lastUpdate}
                            </span>
                        </p>
                    </section>
                }
                <Mosaic className="GalleryOverview__images">
                    {this.images.map(info => <GalleryImageThumb id={info.id} key={info.id} />)}
                </Mosaic>
                <section className="GalleryOverview__navigation">
                    <Link to={routeIndex.path()} className="GalleryOverview__indexLink">
                        <Triangle direction="up" />
                    </Link>
                </section>
            </article>
        );
    }
}
