import * as React from "react";
import { IndexGalleryThumb } from "../index-gallery-thumb";
import "./site-index.scss";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { ServiceGalleries } from "../../services";
import { computed } from "mobx";

@external @observer
export class SiteIndex extends React.Component {
    @inject private serviceGalleries: ServiceGalleries;

    @computed private get galleries() {
        return this.serviceGalleries.all();
    }

    public render() {
        if (!this.galleries) { return <></>; }
        return (
            <article className="SiteIndex">
                <h2 className="SiteIndex__title">Galleries</h2>
                <section className="SiteIndex__galleries">
                    {this.galleries.map(gallery => <IndexGalleryThumb id={gallery.id} key={gallery.id} />)}
                </section>
            </article>
        );
    }
}
