import * as React from "react";
import * as classNames from "classnames";
import "./triangle.scss";

export interface TriangleProps {
    direction: "up" | "down" | "left" | "right";
}

export class Triangle extends React.Component<TriangleProps> {
    private get classes() {
        return classNames([
            "Triangle",
            `Triangle--${this.props.direction}`,
        ]);
    }

    public render() {
        return (
            <div className={this.classes} />
        );
    }
}
