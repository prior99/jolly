import * as React from "react";
import { Link } from "react-router-dom";
import { imageSizeTiny, ModelGallery } from "../../../common";
import { routeGallery } from "../../pages";
import { imageUrl } from "../../utils";
import "./index-gallery-thumb.scss";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { ServiceGalleries, ServiceImages } from "../../services";
import { computed } from "mobx";

@external @observer
export class IndexGalleryThumb extends React.Component<{ id: string }> {
    @inject private serviceGalleries: ServiceGalleries;
    @inject private serviceImages: ServiceImages;

    @computed private get gallery() {
        return this.serviceGalleries.byId(this.props.id);
    }

    @computed private get images() {
        return this.serviceImages.inGallery(this.props.id) || [];
    }

    public render() {
        if (!this.gallery) { return <></>; }
        return (
            <div className="IndexGalleryThumb">
                <Link to={routeGallery.path(this.gallery.id)} className="IndexGalleryThumb__link">
                    <div className="IndexGalleryThumb__name">
                        {this.gallery.name}
                    </div>
                    <div className="IndexGalleryThumb__images">
                        {
                            this.images.slice(0, 3).map(image => (
                                <img
                                    className="IndexGalleryThumb__image"
                                    src={imageUrl(this.gallery.id, image.id, imageSizeTiny)}
                                    key={image.id}
                                />
                            ))
                        }
                    </div>
                </Link>
            </div>
        );
    }
}
