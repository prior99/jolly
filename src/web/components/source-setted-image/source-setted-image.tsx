import * as React from "react";
import { imageSizes, imageSizeHD, ModelImage } from "../../../common";
import { imageUrl } from "../../utils";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { ServiceImages, ServiceGalleries } from "../../services";
import { computed } from "mobx";

export interface SourceSettedImageProps {
    id: string;
    className?: string;
}

@external @observer
export class SourceSettedImage extends React.Component<SourceSettedImageProps> {
    @inject private serviceImages: ServiceImages;
    @inject private serviceGalleries: ServiceGalleries;

    @computed private get image() {
        return this.serviceImages.byId(this.props.id);
    }

    @computed private get gallery() {
        return this.serviceGalleries.byId(this.image.gallery.id);
    }

    public render() {
        if (!this.image || !this.gallery) { return <></>; }
        const ratio = this.image.width / this.image.height;
        const srcSet = imageSizes
            .map(size => `${imageUrl(this.gallery.id, this.image.id, size)} ${Math.round(size * ratio)}w`)
            .reduce((a, b) => `${a}, ${b}`);

        return (
            <img
                src={imageUrl(this.gallery.id, this.image.id, imageSizeHD)}
                srcSet={srcSet}
                className={this.props.className}
            />
        );
    }
}
