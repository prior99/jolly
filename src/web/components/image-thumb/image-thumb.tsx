import * as React from "react";
import { Link } from "react-router-dom";
import * as classNames from "classnames";
import { external, inject } from "tsdi";
import { imageSizeTiny } from "../../../common";
import { imageUrl } from "../../utils";
import "./image-thumb.scss";
import { observer } from "mobx-react";
import { ServiceImages } from "../../services";
import { computed } from "mobx";
import { routeImage } from "../../pages";

export interface ImageThumbProps {
    id: string;
    active: boolean;
}

@external @observer
export class ImageThumb extends React.Component<ImageThumbProps> {
    @inject private serviceImages: ServiceImages;

    @computed get classes() {
        return classNames({
            "ImageThumb": true,
            "ImageThumb--active": this.props.active,
        });
    }

    @computed get linkClasses() {
        return classNames(["ImageThumb__link"]);
    }

    @computed get imageClasses() {
        return classNames(["ImageThumb__image"]);
    }

    @computed private get image() {
        return this.serviceImages.byId(this.props.id);
    }

    public render() {
        if (!this.image) { return <></>; }
        const path = imageUrl(this.image.gallery.id, this.image.id, imageSizeTiny);
        return (
            <div className={this.classes}>
                <Link className={this.linkClasses} to={routeImage.path(this.image.id)}>
                    <img className={this.imageClasses} src={path} alt={this.image.filename} />
                </Link>
            </div>
        );
    }
}
