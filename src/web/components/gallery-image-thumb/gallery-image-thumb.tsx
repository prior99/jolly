import * as React from "react";
import { Link } from "react-router-dom";
import { SourceSettedImage } from "../source-setted-image";
import { routeImage } from "../../pages";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { ServiceImages } from "../../services";
import { computed } from "mobx";
import "./gallery-image-thumb.scss";

@external @observer
export class GalleryImageThumb extends React.Component<{ id: string }> {
    @inject private serviceImages: ServiceImages;

    @computed private get image() {
        return this.serviceImages.byId(this.props.id);
    }

    public render() {
        if (!this.image) { return <></>; }
        return (
            <div className="GalleryImageThumb">
                <Link className="GalleryImageThumb__link" to={routeImage.path(this.image.id)}>
                    <SourceSettedImage className="GalleryImageThumb__image" id={this.image.id}/>
                </Link>
            </div>
        );
    }
}
