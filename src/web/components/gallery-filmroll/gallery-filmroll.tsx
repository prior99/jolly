import * as React from "react";
import { Link } from "react-router-dom";
import * as classNames from "classnames";
import { observer } from "mobx-react";
import { external, inject } from "tsdi";
import { ImageThumb } from "../image-thumb";
import { ModelGallery, ModelImage } from "../../../common";
import { Hint } from "../hint";
import { Triangle } from "../triangle";
import { routeImage, routeGallery } from "../../pages";
import "./gallery-filmroll.scss";
import { ServiceImages } from "../../services";
import { computed } from "mobx";

export interface GalleryFilmrollProps {
    id: string;
    activeId: string;
    className: string;
}

@external @observer
export class GalleryFilmroll extends React.Component<GalleryFilmrollProps> {
    @inject private serviceImages: ServiceImages;

    @computed private get classes() {
        return classNames(["GalleryFilmroll", this.props.className]);
    }

    @computed private get images() {
        const images = this.serviceImages.inGallery(this.props.id) || [];
        if (!this.image) { return []; }
        const currentIndex = images.findIndex(image => image.id === this.image.id);
        const desiredIndex = Math.floor(images.length / 2);
        return [
            ...images.slice(currentIndex - desiredIndex, images.length),
            ...images.slice(0, currentIndex - desiredIndex),
        ];
    }

    @computed private get nextImage() {
        return this.serviceImages.next(this.props.activeId);
    }

    @computed private get previousImage() {
        return this.serviceImages.previous(this.props.activeId);
    }

    @computed private get image() {
        return this.serviceImages.byId(this.props.activeId);
    }

    public render() {
        if (!this.image || !this.previousImage || !this.nextImage || !this.images) { return <></>; }
        return (
            <section className={this.classes}>
                <div className="GalleryFilmroll__navigation">
                    <Link to={routeImage.path(this.previousImage.id)} className="GalleryFilmroll__previousLink">
                        <Triangle direction="left" />
                    </Link>
                    <Link to={routeImage.path(this.nextImage.id)} className="GalleryFilmroll__nextLink">
                        <Triangle direction="right" />
                    </Link>
                    <Link to={routeGallery.path(this.image.gallery.id)} className="GalleryFilmroll__galleryLink">
                        <Triangle direction="up" />
                    </Link>
                </div>
                <div className="GalleryFilmroll__images">
                    {
                        this.images.map(image => {
                            return (
                                <ImageThumb
                                    active={this.image.id === image.id}
                                    id={image.id}
                                    key={image.id}
                                />
                            );
                        })
                    }
                </div>
                <Hint className="GalleryFilmroll__hint" />
            </section>
        );
    }
}
