import * as React from "react";
import * as classNames from "classnames";
import "./mosaic.scss";

export interface MosaicProps {
    size?: "small" | "large";
    className?: string;
}

export class Mosaic extends React.Component<MosaicProps> {
    protected static defaultProps = {
        size: "small",
    };

    private get children() {
        if (Array.isArray(this.props.children)) {
            return this.props.children.map((child, index) => (
                <div className="Mosaic__column" key={index}>
                    {child}
                </div>
            ));
        }
        return (
            <div className="Mosaic__column">
                {this.props.children}
            </div>
        );
    }

    public render() {
        return (
            <div className={classNames("Mosaic", `Mosaic--${this.props.size}`, this.props.className)}>
                {this.children}
            </div>
        );
    }
}
