import * as React from "react";
import { Link } from "react-router-dom";
import { observer } from "mobx-react";
import * as classNames from "classnames";
import { ModelGallery, ModelImage  } from "../../../common";
import { routeImage } from "../../pages";
import { GalleryFilmroll } from "../gallery-filmroll";
import { SourceSettedImage } from "../source-setted-image";
import "./fullscreen-image.scss";
import { external, inject } from "tsdi";
import { ServiceImages } from "../../services";
import { computed } from "mobx";

@external @observer
export class FullscreenImage extends React.Component<{ id: string }> {
    @inject private serviceImages: ServiceImages;

    @computed private get classes() {
        return classNames(["FullscreenImage"]);
    }

    @computed private get nextImage() {
        return this.serviceImages.next(this.props.id);
    }

    @computed private get image() {
        return this.serviceImages.byId(this.props.id);
    }

    @computed private get galleryId() {
        if (!this.image) { return; }
        return this.image.gallery.id;
    }

    public render() {
        if (!this.image || !this.nextImage) { return <></>; }
        return (
            <article className={this.classes}>
                <Link to={routeImage.path(this.nextImage.id)}>
                    <SourceSettedImage id={this.props.id} className="FullscreenImage__image" />
                </Link>
                <GalleryFilmroll
                    id={this.galleryId}
                    activeId={this.props.id}
                    className="FullscreenImage__filmroll"
                />
            </article>
        );
    }
}
