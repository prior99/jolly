import * as React from "react";
import { addRoute, RouteProps } from "../../routing";
import { FullscreenImage } from "../../components";

export class PageImage extends React.Component<RouteProps<{ id: string }>> {
    public render() {
        return <FullscreenImage id={this.props.match.params.id} />;
    }
}

export const routeImage = {
    path: (id: string) => `/image/${id}`,
    pattern: "/image/:id",
    component: PageImage,
};

addRoute(routeImage);
