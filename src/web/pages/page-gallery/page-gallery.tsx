import * as React from "react";
import { addRoute, RouteProps } from "../../routing";
import { GalleryOverview } from "../../components";

export class PageGallery extends React.Component<RouteProps<{ id: string }>> {
    public render() {
        return <GalleryOverview id={this.props.match.params.id} />;
    }
}

export const routeGallery = {
    path: (id: string) => `/gallery/${id}`,
    pattern: "/gallery/:id",
    component: PageGallery,
};

addRoute(routeGallery);
