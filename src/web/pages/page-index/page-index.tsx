import * as React from "react";
import { addRoute, RouteProps } from "../../routing";
import { SiteIndex } from "../../components";

export class PageIndex extends React.Component<RouteProps<{}>> {
    public render() {
        return <SiteIndex />;
    }
}

export const routeIndex = {
    path: () => "/index",
    pattern: "/index",
    component: PageIndex,
};

addRoute(routeIndex);
