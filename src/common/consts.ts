export const imagesDirName = "images";

export const imageSizeThumb = 60;
export const imageSizeTiny = 240;
export const imageSizeSmall = 720;
export const imageSizeHD = 1080;
export const imageSize4k = 2160;

export const imageSizes = [imageSizeTiny, imageSizeSmall, imageSizeHD, imageSize4k];

export function imageFilename(id: string, size: number) {
    return `${id}_${size}.jpeg`;
}
