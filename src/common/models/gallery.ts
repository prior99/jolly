import { Entity, PrimaryColumn, Column, ManyToOne, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ModelImage } from "./image";

@Entity({ name: "gallery" })
export class ModelGallery {
    @PrimaryColumn()
    public id: string;

    @Column()
    public name: string;

    @Column()
    public description: string;

    @CreateDateColumn()
    public created: Date;

    @UpdateDateColumn()
    public updated: Date;

    @OneToMany(() => ModelImage, image => image.gallery)
    public images: ModelImage[];
}
