import { Entity, PrimaryColumn, Column, ManyToOne, OneToMany, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { ModelGallery } from "./gallery";

@Entity({ name: "image" })
export class ModelImage {
    @PrimaryColumn()
    public id: string;

    @Column()
    public filename: string;

    @Column()
    public format: string;

    @Column("int")
    public width: number;

    @Column("int")
    public height: number;

    @CreateDateColumn()
    public created: Date;

    @UpdateDateColumn()
    public updated: Date;

    @ManyToOne(() => ModelGallery, gallery => gallery.images)
    public gallery: ModelGallery;
}
