export * from "./gallery";
export * from "./image";
import { ModelGallery } from "./gallery";
import { ModelImage } from "./image";

export const entities = [
    ModelGallery,
    ModelImage,
];
